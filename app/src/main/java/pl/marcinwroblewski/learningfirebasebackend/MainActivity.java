package pl.marcinwroblewski.learningfirebasebackend;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.models.PieModel;

public class MainActivity extends AppCompatActivity {

    private PieChart pieChart;
    private DatabaseReference firebaseDatabase;
    private ChildEventListener childEventListener;

    private ArrayMap<String, MarketSharePart> vendorsData;

    private int[] chartColors = new int[]{
            Color.parseColor("#e91e63"),
            Color.parseColor("#3f51b5"),
            Color.parseColor("#00bcd4"),
            Color.parseColor("#8bc34a"),
            Color.parseColor("#ffc107"),
            Color.parseColor("#795548"),
            Color.parseColor("#607d8b")
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("vendors");

        vendorsData = new ArrayMap<>();

        setup();
    }

    private void setup() {
        setupPieChart();
        setupOnDatabaseListener();
    }

    private void setupPieChart() {
        pieChart = (PieChart) findViewById(R.id.pie_chart);
    }

    private void updatePieChartData(boolean animationRequired) {

        (findViewById(R.id.data_loading)).setVisibility(View.GONE);

        pieChart.clearChart();

        for(int i = 0; i < vendorsData.size(); i++) {
            MarketSharePart part =  vendorsData.get(vendorsData.keySet().toArray()[i]);

            pieChart.addPieSlice(
                    new PieModel(
                            part.getVendorName(),
                            (float) part.getMarketShare(),
                            chartColors[i%chartColors.length]));
        }

        if(animationRequired) pieChart.startAnimation();
    }

    private void setupOnDatabaseListener() {
        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                vendorsData.put(dataSnapshot.getKey(),
                        new MarketSharePart(
                                dataSnapshot.child("name").getValue(String.class),
                                dataSnapshot.child("marketShare").getValue(Double.TYPE)
                        ));

                updatePieChartData(true);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                vendorsData.setValueAt(vendorsData.indexOfKey(dataSnapshot.getKey()),
                        new MarketSharePart(
                                dataSnapshot.child("name").getValue(String.class),
                                dataSnapshot.child("marketShare").getValue(Double.TYPE)
                        ));

                updatePieChartData(false);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                vendorsData.remove(dataSnapshot.getKey());

                updatePieChartData(false);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
            }
        };
        firebaseDatabase.addChildEventListener(childEventListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = new MenuInflater(getApplicationContext());
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                logout();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        FirebaseAuth.getInstance().signOut();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseAuth.getInstance().addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(firebaseAuth.getCurrentUser() == null) {
                    Intent intent = new Intent(getApplicationContext(), AuthActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebaseDatabase.removeEventListener(childEventListener);
    }
}
