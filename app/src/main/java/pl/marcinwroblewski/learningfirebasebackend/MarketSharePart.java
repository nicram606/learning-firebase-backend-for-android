package pl.marcinwroblewski.learningfirebasebackend;

/**
 * Created by Marcin Wróblewski on 08.03.2017.
 */

public class MarketSharePart {

    private String vendorName;
    private double marketShare;


    public MarketSharePart(String vendorName, double marketShare) {
        this.vendorName = vendorName;
        this.marketShare = marketShare;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public double getMarketShare() {
        return marketShare;
    }

    public void setMarketShare(double marketShare) {
        this.marketShare = marketShare;
    }
}
